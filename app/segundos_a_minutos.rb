class SegundosAMinutos
  SEGUNDOS_POR_MINUTO = 60.0
  def calcular(segundos)
    (segundos / SEGUNDOS_POR_MINUTO).round(0)
  end
end
