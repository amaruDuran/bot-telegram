require 'spec_helper'
require 'web_mock'
require_relative '../app/conexion_here'

def then_i_get_conection_for(transport_mode, origin, destination)
  stub_request(:get, "https://router.hereapi.com/v8/routes?apikey=&destination=#{destination}&origin=#{origin}&return=summary&transportMode=#{transport_mode}")
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: '', headers: {})
end

describe 'ConexionHere' do
  it 'Conexion indicando todos los parámetros es correcta' do
    transport_mode = 'car'
    origin = '-34.6060%2C-58.4570'
    destination = '-34.6094484%2C-58.4558905'

    then_i_get_conection_for(transport_mode, origin, destination)
  end
end
