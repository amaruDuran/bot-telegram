class ParserRespuestaHere
  def initialize(a_response_body)
    @body = a_response_body
  end

  def duracion_en_segundos
    json_response = JSON.parse(@body)
    routes_list = json_response['routes']
    first_route = routes_list[0]
    first_route['sections'][0]['summary']['duration']
  end
end
