class ConexionHere
  API_KEY = ENV['HERE_API_KEY']

  def conectarme_con(latitud1, longitud1, latitud2, longitud2, transporte = 'car')
    origen = "#{latitud1}%2C#{longitud1}"
    destino = "#{latitud2}%2C#{longitud2}"

    conexion = Faraday::Connection.new 'https://router.hereapi.com'

    url = "/v8/routes?transportMode=#{transporte}&origin=#{origen}&destination=#{destino}&return=summary&apikey=#{API_KEY}"

    respuesta = conexion.get url

    if respuesta.success?
      respuesta
    else
      raise ConexionHereException
    end
  end
end
